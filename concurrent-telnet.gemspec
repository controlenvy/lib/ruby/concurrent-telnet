# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "concurrent/telnet/version"

Gem::Specification.new do |spec|
  spec.name          = "concurrent-telnet"
  spec.version       = Concurrent::Telnet::VERSION
  spec.authors       = ["David Massey"]
  spec.email         = ["david@controlenvy.com"]
  spec.date          = "2019-02-28"

  spec.summary       = %q{Expands Net::Telnet client functionality for ConcurrentIO.}
  spec.description   = %q{Expands Net::Telnet client functionality for ConcurrentIO. Tested with selector == EventMachine.}
  spec.homepage      = "https://gitlab.com/controlenvy/lib/ruby/concurrent-telnet"
  spec.license       = "LGPL-3.0+"

  spec.files         = Dir["lib/**/*.rb"]
  spec.require_paths = ["lib"]

  spec.required_ruby_version = '>= 2.3.0'

  spec.add_development_dependency "bundler", "~> 0"
  spec.add_development_dependency "rake", "~> 0"
  spec.add_development_dependency "rspec", "~> 0"

end
