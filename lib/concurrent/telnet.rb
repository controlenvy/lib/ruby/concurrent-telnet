# = concurrent/telnet.rb - Net::Telnet client library adapted for ConcurrentIO
#
# Adapter:: David Massey <david@controlenvy.com>
# Author:: Wakou Aoyama <wakou@ruby-lang.org>
# Documentation:: William Webber and Wakou Aoyama
#
# This file holds an adaptation of class Net::Telnet, which provides client-side
# telnet functionality suitable to use with ConcurrentIO.
#
# For documentation, see Concurrent::Telnet.
#

require "net/protocol"
require 'concurrent/telnet/constants'

module Concurrent
  class Telnet
    include Concurrent::Telnet::Constants

    attr_reader :sock

    def initialize(**kwargs)
      @options = kwargs

      @options[:Host]        ||= "localhost"
      @options[:Port]        ||= 23
      @options[:Telnetmode]  ||= false # This is a departure from Net::Telnet (default true).

      @options[:Prompt]      ||= /[$%#>] \z/n
      @options[:Timeout]     ||= 10
      @options[:Waittime]    ||= 0
      @options[:BufferSize]  ||= 1024 * 1024 # Default from Net::Telnet
      @options[:FailEOF]     ||= true

      @options[:Binmode]     ||= false

      puts "\n\n\n***Initializing telnet***\n  @options: #{@options.inspect}"

      # Argument type checks
      case false
      when (@options[:Binmode] == true || @options[:Binmode] == false)
        raise ArgumentError, "Binmode option must be true or false"
      when (@options[:Telnetmode] == true || @options[:Telnetmode] == false)
        raise ArgumentError, "Telnetmode option must be true or false"
      end

      @telnet_option = { SGA: false, BINARY: false }

      if @options.has_key?(:Proxy)
        case true
        when @options[:Proxy].kind_of?(IO)
          @sock = @options[:Proxy]
        when @options[:Proxy].respond_to?("sock") && @options[:Proxy].sock.kind_of?(IO)
          @sock = @options[:Proxy].sock
        else
          raise ArgumentError, "Proxy must be an instance of IO or contain an IO-type 'sock' property."
        end
      else
        begin
          if @options[:Timeout] == false
            @sock = TCPSocket.open(@options[:Host], @options[:Port])
          else
            Timeout.timeout(@options[:Timeout], Net::OpenTimeout) do
              @sock = TCPSocket.open(@options[:Host], @options[:Port])
            end
          end
        rescue Net::OpenTimeout
          raise Net::OpenTimeout, "Timed out after #{@options[:Timeout]}s while connecting to #{@options[:Host]} #{@options[:Port]}."
        end

        @sock.sync = true
        @sock.binmode
      end
    end

    #
    # Getters and Setters
    #
    def telnetmode(mode = nil)
      case mode
      when nil
        @options[:Telnetmode]
      when true, false
        @options[:Telnetmode] = mode
      else
        raise ArgumentError, "argument must be true or false, or missing"
      end
    end

    def telnetmode=(mode)
      if (true == mode or false == mode)
        @options[:Telnetmode] = mode
      else
        raise ArgumentError, "argument must be true or false"
      end
    end

    def binmode(mode = nil)
      case mode
      when nil
        @options[:Binmode]
      when true, false
        @options[:Binmode] = mode
      else
        raise ArgumentError, "argument must be true or false"
      end
    end

    def binmode=(mode)
      if (true == mode or false == mode)
        @options[:Binmode] = mode
      else
        raise ArgumentError, "argument must be true or false"
      end
    end

    def fileno # A ConcurrentIO requirement.
      case true
      when @sock.respond_to?(:fileno)
        return @sock.fileno
      else
        return @sock
      end
    end

    #
    # Telnet Protocol Implementation
    #
    # Performs newline conversion and detects telnet command sequences.
    # Called automatically by #read_nonblock().  You should only use this
    # method yourself if you have read input directly using sysread()
    # or similar, and even then only if in telnet mode.
    #
    def preprocess(message)
      # combine CR+NULL into CR
      message = message.gsub(/#{CR}#{NULL}/no, CR) if @options[:Telnetmode]

      # combine EOL into "\n"
      message = message.gsub(/#{EOL}/no, "\n") unless @options[:Binmode]

      # remove NULL
      message = message.gsub(/#{NULL}/no, '') unless @options[:Binmode]

      message.gsub(
        %r"
          #{IAC}
          ([#{IAC}#{AO}#{AYT}#{DM}#{IP}#{NOP}]|
            [#{DO}#{DONT}#{WILL}#{WONT}][#{OPT_BINARY}-#{OPT_NEW_ENVIRON}#{OPT_EXOPL}]|
            #{SB}[^#{IAC}]*#{IAC}#{SE})
        "xno
      ) do
        if IAC == $1  # handle escaped IAC characters
          return IAC
        elsif AYT == $1  # respond to "IAC AYT" (are you there)
          send_raw("nobody here but us pigeons" + EOL)
        elsif DO[0] == $1[0]  # respond to "IAC DO x"
          if OPT_BINARY[0] == $1[1]
            @telnet_option[:BINARY] = true
            send_raw(IAC + WILL + OPT_BINARY)
          else
            send_raw(IAC + WONT + $1[1..1])
          end
        elsif DONT[0] == $1[0]  # respond to "IAC DON'T x" with "IAC WON'T x"
          send_raw(IAC + WONT + $1[1..1])
        elsif WILL[0] == $1[0]  # respond to "IAC WILL x"
          if OPT_BINARY[0] == $1[1]
            send_raw(IAC + DO + OPT_BINARY)
          elsif OPT_ECHO[0] == $1[1]
            send_raw(IAC + DO + OPT_ECHO)
          elsif OPT_SGA[0]  == $1[1]
            @telnet_option[:SGA] = true
            send_raw(IAC + DO + OPT_SGA)
          else
            send_raw(IAC + DONT + $1[1..1])
          end
        elsif WONT[0] == $1[0]  # respond to "IAC WON'T x"
          if OPT_ECHO[0] == $1[1]
            send_raw(IAC + DONT + OPT_ECHO)
          elsif OPT_SGA[0]  == $1[1]
            @telnet_option[:SGA] = false
            send_raw(IAC + DONT + OPT_SGA)
          else
            send_raw(IAC + DONT + $1[1..1])
          end
        end

        return ""
      end
    end

    #
    # Reads received data using @sock.read_nonblock()
    #
    # Performs a non-blocking read on the socket and does not return until an expected
    # regex pattern is true.
    #
    def expect(expected = @options[:Prompt], **kwargs)
      #epxected    = kwargs.has_key?(:Expected)   ? kwargs[:Expected]    : @options[:Prompt]
      time_out    = kwargs.has_key?(:Timeout)    ? kwargs[:Timeout]     : @options[:Timeout]
      wait_time   = kwargs.has_key?(:Waittime)   ? kwargs[:Waittime]    : @options[:Waittime]
      fail_eof    = kwargs.has_key?(:FailEOF)    ? kwargs[:FailEOF]     : @options[:FailEOF]
      buffer_size = kwargs.has_key?(:BufferSize) ? kwargs[:BufferSize]  : @options[:BufferSize]

      time_out = nil if time_out == false

      message   = ''
      buffer    = ''
      remainder = ''

      until(expected === message and not @sock.wait_readable(wait_time))
        unless @sock.wait_readable(time_out)
          raise IO::WaitReadable, "timed out while waiting for more data"
        end

        begin
          received = @sock.read_nonblock(buffer_size)

          if @options[:Telnetmode]
            received = remainder + received

            if Integer(received.rindex(/#{IAC}#{SE}/no) || 0) <
               Integer(received.rindex(/#{IAC}#{SB}/no) || 0)
              buffer = preprocess(c[0 ... received.rindex(/#{IAC}#{SB}/no)])
              remainder = received[received.rindex(/#{IAC}#{SB}/no) .. -1]
            elsif pt = received.rindex(/#{IAC}[^#{IAC}#{AO}#{AYT}#{DM}#{IP}#{NOP}]?\z/no) ||
                       received.rindex(/\r\z/no)
              buffer = preprocess(received[0 ... pt])
              remainder = received[pt .. -1]
            else
              buffer = preprocess(received)
              remainder = ''
            end
          else # Not Telnetmode.
            buffer = remainder + received
            remainder = ''

            unless @options[:Binmode]
              if pt = buffer.rindex(/\r\z/no)
                buffer = buffer[0 ... pt]
                remainder = buffer[pt .. -1]
              end

              buffer.gsub!(/#{EOL}/no, "\n")
            end
          end

          message += buffer
        rescue EOFError # End of file reached
          raise if fail_eof
          message = nil if message == ''
          break
        end
      end

      return message
    end

    #
    # Reads received data until the prompt is reached.
    #
    # Required for ConcurrentIO.
    #
    def read_nonblock(buffer_size)
      expect @options[:Prompt], BufferSize: buffer_size
    end

    #
    # Writes message to the host.
    #
    # Does not perform any conversions on message.
    #
    def send_raw(message)
      length = message.length
      while 0 < length
        @sock.wait_writable
        length -= @sock.syswrite(message[-length..-1])
      end
    end

    #
    # Sends a message to the host.
    #
    # This does _not_ automatically append a newline to the message.  Embedded
    # newlines may be converted and telnet command sequences escaped
    # depending upon the values of telnetmode, binmode, and telnet options
    # set by the host.
    #
    def send(message)
      message = message.gsub(/#{IAC}/no, IAC + IAC) if @options[:Telnetmode]

      if @options[:Binmode]
        send_raw message
      else
        if @telnet_option[:BINARY] and @telnet_option[:SGA]
          # IAC WILL SGA IAC DO BIN send EOL --> CR
          send_raw message.gsub(/\n/n, CR)
        elsif @telnet_option[:SGA]
          # IAC WILL SGA send EOL --> CR+NULL
          send_raw message.gsub(/\n/n, CR + NULL)
        else
          # NONE send EOL --> CR+LF
          send_raw message.gsub(/\n/n, EOL)
        end
      end
    end

    #
    # Sends a message to the host.
    #
    # Same as #send(), but appends a newline to the message.
    #
    def send_line(message)
      send "#{message}\n"
    end

    #
    # Send a command to the host and waits for a response.
    #
    # More exactly, sends a string to the host, and reads in all received
    # data until it sees the prompt or expected sequence.
    #
    # Expected:: a regular expression, the sequence to look for in
    #         the received data before returning.  If not specified,
    #         the Prompt option value specified when this instance
    #         was created will be used, or, failing that, the default
    #         prompt of /[$%#>] \z/n.
    # Timeout:: the seconds to wait for data from the host before raising
    #           a Timeout error.  If not specified, the Timeout option
    #           value specified when this instance was created will be
    #           used, or, failing that, the default value of 10 seconds.
    #
    # The command or other string will have the newline sequence appended
    # to it.
    #
    def send_and_expect(message, **kwargs)
      msg_options = kwargs

      prompt    = kwargs.has_key?(:Expected) ? kwargs[:Expected]  : @options[:Prompt]
      time_out  = kwargs.has_key?(:Timeout)  ? kwargs[:Timeout]   : @options[:Timeout]
      fail_eof  = kwargs.has_key?(:FailEOF)  ? kwargs[:FailEOF]   : @options[:FailEOF]

      send_line message
      expect prompt, Timeout: time_out, FailEOF: fail_eof
    end

    def login(username, password = nil, **kwargs)
      login_prompt    = kwargs.has_key?(:LoginPrompt)    ? kwargs[:LoginPrompt]    : /[Ll]ogin[: ]*\z/n
      password_prompt = kwargs.has_key?(:PasswordPrompt) ? kwargs[:PasswordPrompt] : /[Pp]ass(?:word|phrase)[: ]*\z/n

      expect login_prompt

      if password
        send_and_expect username, Expected: password_prompt
        send_and_expect password
      else
        send_and_expect username
      end
    end

    # Closes the connection
    def close
      @sock.close
    end
  end
end
