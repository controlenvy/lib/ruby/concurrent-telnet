# Concurrent::Telnet

Provides telnet client functionality with ConcurrentIO (tested with EventMachine as the selector).

Telnet protocol implementation was adapted from [Net::Telnet](https://github.com/ruby/net-telnet). Some features have been stripped out form Net::Telnet (i.e. block_given, dump, and log). Most has been modified to increase readability or to satisify a ConcurrentIO method requirement.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'concurrent-telnet'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install concurrent-telnet

## Usage

### Log in and send a message

```ruby
localhost = Net::Telnet::new Host: "localhost",
                             Timeout: 10,
                             Prompt: /[$%#>] \z/n
localhost.login "username", "password"
localhost.send "message"
localhost.close
```

### Log in, send a message, and print the reply

```ruby
localhost = Net::Telnet::new Host: "localhost",
                             Telnetmode: false,
                             Binmode: true,
                             Timeout: 10,
                             Prompt: /[$%#>] \z/n
localhost.login "username", "password"
puts localhost.send_and_expect "message"
localhost.close
```

## Contributing

1. Clone it ( https://gitlab.com/controlenvy/lib/ruby/concurrent-telnet )
2. Create an issue and issue branch for your contribution
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-contribution`)
5. Create a new Merge Request on the issue
